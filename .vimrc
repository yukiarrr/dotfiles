"----------------------------------------------------------
" 読み込みエンコードオプション（先頭に記述必須）
"----------------------------------------------------------

" 読み込み時の文字コードの設定
set encoding=utf-8

" Vim Script内でマルチバイトを使う場合の設定
scriptencoding utf-8

"----------------------------------------------------------
" プラグイン
"----------------------------------------------------------

" powerline
python3 from powerline.vim import setup as powerline_setup
python3 powerline_setup()
python3 del powerline_setup

" vim-plug
call plug#begin('~/.vim/plugged')

" ペースト時にインデント崩れが起きないようにする
Plug 'ConradIrwin/vim-bracketed-paste'

" インデントを可視化する
Plug 'Yggdroot/indentLine'

" ファイルをtree表示してくれる
Plug 'scrooloose/nerdtree'

" コメントアウトを手軽にできるようにする
Plug 'scrooloose/nerdcommenter'

" git管理機能を追加
Plug 'tpope/vim-fugitive'

" 行末の半角スペースを可視化
Plug 'bronson/vim-trailing-whitespace'

" fzfを使いインタラクティブに選択できるようにする
Plug '/usr/local/opt/fzf'
Plug 'junegunn/fzf.vim'

" Prettierを実行できるようになる
Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }

call plug#end()

"----------------------------------------------------------
" オプション
"----------------------------------------------------------

" 保存時の文字コードの設定
set fileencoding=utf-8

" <Leader>にスペースを使用する
let mapleader = "\<Space>"

" ステータスラインを常に表示
set laststatus=2

" コマンドモードの補完
set wildmenu

" 保存するコマンド履歴の数
set history=5000

" 行番号を表示
set number

" タブ入力を複数の空白入力に置き換える
set expandtab

" 画面上でタブ文字が占める幅
set tabstop=4

" 連続した空白に対してタブキーやバックスペースキーでカーソルが動く幅
set softtabstop=4

 " 改行時に前の行のインデントを継続する
set autoindent

" 改行時に前の行の構文をチェックし次の行のインデントを増減する
set smartindent

" smartindentで増減する幅
set shiftwidth=4

" インクリメンタルサーチ. １文字入力毎に検索を行う
set incsearch

" 検索パターンに大文字小文字を区別しない
set ignorecase

" 検索パターンに大文字を含んでいたら大文字小文字を区別する
set smartcase

" 検索結果をハイライト
set hlsearch

" バッファスクロール
set mouse=a

" スワップファイルを作らない
set noswapfile

" カーソルラインをハイライト
set cursorline

" backspaceが効くようにする
set backspace=indent,eol,start

" クリップボードにコピーできるようにする
set clipboard=unnamed,autoselect

" NERDTreeで隠しファイルを表示する
let NERDTreeShowHidden=1

" NERDTreeを起動時に表示し、ファイル側にカーソルを合わせる
autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p

" コメントの色を変更する
hi Comment ctermfg=DarkGreen

" 全角スペースの表示
function! ZenkakuSpace()
    highlight ZenkakuSpace cterm=underline ctermfg=lightblue guibg=darkgray
endfunction
if has('syntax')
    augroup ZenkakuSpace
        autocmd!
        autocmd ColorScheme * call ZenkakuSpace()
        autocmd VimEnter,WinEnter,BufRead * let w:m1=matchadd('ZenkakuSpace', '　')
    augroup END
    call ZenkakuSpace()
endif
