#!/bin/bash

brew bundle dump --global --force
pip3 freeze > requirements.txt
