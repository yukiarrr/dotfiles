#!/bin/bash

# brewのドキュメントを参考に必要なものをインストール
# https://docs.brew.sh/Homebrew-on-Linux

if [ -f /etc/lsb-release -o -f /etc/debian_release ]; then
    sudo apt-get update && sudo apt-get install build-essential curl file git
elif [ -f /etc/redhat-release -o -f /etc/fedora-release ]; then
    sudo yum groupinstall 'Development Tools'
    sudo yum install curl file git
    sudo yum install libxcrypt-compat
fi

if ! type brew > /dev/null 2>&1; then
    if [ "$(uname)" = 'Darwin' ]; then
        /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    elif [ "$(uname -s | cut -c 1-5)" = 'Linux' ]; then
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"
    fi
fi
