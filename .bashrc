# linux用に必要な処理を記述
if [ "$(uname -s | cut -c 1-5)" = 'Linux' ]; then
    export PATH=/home/linuxbrew/.linuxbrew/Homebrew/Library/Homebrew/vendor/portable-ruby/current/bin:$PATH
    [ -d /home/linuxbrew/.linuxbrew ] && eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
fi

# 起動時のみfish shellを実行する
case $- in
    *i*) type fish > /dev/null 2>&1 && exec fish;;
esac
