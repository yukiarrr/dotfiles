#----------------------------------------------------------
# 強制設定
#----------------------------------------------------------

# pythonでpython3を使用する設定(主にbass用)
set -x PATH (brew --prefix)/opt/python3/libexec/bin $PATH

# pipenvでプロジェクト直下に仮想環境を作る
set -x PIPENV_VENV_IN_PROJECT 1

# nodebrew
set -x PATH $HOME/.nodebrew/current/bin $PATH

# google-cloud
type bass > /dev/null 2>&1 && [ -r /usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/path.bash.inc ] && bass source /usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/path.bash.inc
type bass > /dev/null 2>&1 && [ -r /usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/completion.bash.inc ] && bass source /usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/completion.bash.inc

# powerline
set fish_function_path $fish_function_path (python3 -c 'import site; print(site.getsitepackages()[0])')/powerline/bindings/fish
type powerline-setup > /dev/null 2>&1 && powerline-setup

# fzf
set FZF_LEGACY_KEYBINDINGS 0

#----------------------------------------------------------
# 独自設定
#----------------------------------------------------------

# fzfのデフォルトオプションの設定
set -x FZF_DEFAULT_OPTS '--height 40% --layout=reverse --border --bind alt-up:preview-up,alt-down:preview-down'

# 色の設定
set -x LSCOLORS gxfxcxdxbxegedabagacad
set fish_color_command 9682f0
set fish_color_param b0a7f5

#----------------------------------------------------------
# 独自コマンド
#----------------------------------------------------------

# 移動先をインタラクティブに選択できるcdコマンド
functions --copy cd standard_cd
function cd
    if test -z $argv
        set argv (find . -maxdepth 2 -type d | fzf)
    else if test $argv = '-'
        set argv (z -lt | cut -c 12- | fzf)
    end
    if test -z $argv
        return
    end
    standard_cd $argv; and ls -a
end

# ファイル一覧をプレビュー付きで確認できるlsコマンド
functions --copy ls standard_ls
function ls
    if test -z $argv
        standard_ls -a $argv | fzf --preview 'cat {}' --height 90%
        return 0
    else
        standard_ls $argv
    end
end

# 対象のディレクトリを選択して移動できるghqコマンド
function gd
    cd (ghq root)/(ghq list | fzf)
end
