#!/bin/bash

# findでは頭に「./」が付いているので正規表現で除去
for f in $(find . -type f | sed s#^\./##g); do
    [[ $f =~ .git/ ]] && continue
    [ $f = '.gitignore' ] && continue
    [ $f = 'README.md' ] && continue
    [ $f = 'brew_setup.sh' ] && continue
    [ $f = 'init.sh' ] && continue
    [ $f = 'link.sh' ] && continue
    [ $f = 'setup.sh' ] && continue
    [ $f = 'update.sh' ] && continue
    [ $f = 'requirements.txt' ] && continue

    # ディレクトリがないなら作成する
    mkdir -p $HOME/$(dirname $f)
    # ファイルから絶対パスを得てリンクさせる
    abspath=$(cd $(dirname $f) && pwd)/$(basename $f)
    ln -sfnv $abspath $HOME/$f
done
