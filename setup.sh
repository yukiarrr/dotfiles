#!/bin/bash

# シンボリックリンクを作成する
./link.sh
. ~/.bash_profile

# パッケージマネージャー本体をインストールする
./brew_setup.sh
[ -e ~/.config/fish/functions/fisher.fish ] || curl https://git.io/fisher --create-dirs -sLo ~/.config/fish/functions/fisher.fish
[ -e ~/.vim/autoload/plug.vim ] || curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
. ~/.bash_profile

# brewで管理しているパッケージをインストールする
brew bundle --global
. ~/.bash_profile

# fisherで管理しているパッケージをインストールする
fish -c fisher

# pipで管理しているパッケージをインストールする
pip3 install -r requirements.txt

# yarnで管理しているパッケージをインストールする
cd $(yarn global dir)
yarn install
cd -

# 必要なフォントをインストールする
git clone https://github.com/powerline/fonts.git --depth=1 ~/fonts
~/fonts/install.sh
rm -rf ~/fonts

# 最後にシェルを再起動する
exec $SHELL -l
