# dotfiles

## Setup

`sh -c "$(curl -fsSL https://gitlab.com/yukiarrr/dotfiles/raw/master/init.sh)"`

or

`git clone https://gitlab.com/yukiarrr/dotfiles.git && cd dotfiles && ./setup.sh`

## Support

- Linux
- Mac
- Windows (Windows Subsystem for Linux)
