#!/bin/bash

read -p 'dotfilesのリポジトリをダウンロードしsetupしますか？ [yes/no]:' yn
if [ "$yn" != 'yes' ]; then
    echo 'Cancel'
    exit
fi

curl -Lo ~/dotfiles-master.tar.gz https://gitlab.com/yukiarrr/dotfiles/-/archive/master/dotfiles-master.tar.gz
mkdir ~/dotfiles
tar -xzvf ~/dotfiles-master.tar.gz -C ~/dotfiles --strip-components 1
rm -f ~/dotfiles-master.tar.gz

cd ~/dotfiles
./setup.sh
